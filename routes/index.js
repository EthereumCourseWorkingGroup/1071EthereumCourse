const express = require('express')
const router = express.Router()

const token = require('../lib/token.js')
const web3 = require('../lib/web3.js')

const nosql = require('../nosql/nosql.js')


/* GET home page. */
router.get('/', async function (req, res, next) {
    let name
    let balance
    if (req.session.uid) {
        let user = nosql.getUserByUid(req.session.uid)
        name = user.name
        balance = await token.tokenBalance(user.Ethereum)
    }
    var projects = require('../nosql/table/project.json')
    var keyList = Object.keys(projects)
    var keyLength = keyList.length
    res.render('index', {
        pass: true,
        name: name,
        balance: balance,
        projects: projects,
        keyList: keyList,
        keyLength: keyLength
    })
})

//登入頁面
router.get('/login', function (req, res, next) {
    if (req.session.name) {
        res.redirect('/')
    }
    res.render('login', { name: null, balance: null, data: "" })
})

//註冊頁面
router.get('/signup', function (req, res, next) {
    if (req.session.name) {
        res.redirect('/')
    }
    res.render('signup', {})
})

//登出
router.get('/signout', function (req, res, next) {
    req.session.destroy()
    res.redirect('/')
})

//捐款頁面
router.get('/fund_donating', async function (req, res, next) {
    let name
    let balance
    if (req.session.uid) {
        let user = nosql.getUserByUid(req.session.uid)
        name = user.name
        balance = await token.tokenBalance(user.Ethereum)
    }
    res.render('fund_donating', { name, balance })
})

//發起計畫頁面
router.get('/fund_raising', async function (req, res, next) {
    let name
    let balance
    if (req.session.uid) {
        let user = nosql.getUserByUid(req.session.uid)
        name = user.name
        balance = await token.tokenBalance(user.Ethereum)
    }
    res.render('fund_raising', { name, balance })
})

//儲值頁面
router.get('/buy_token', async function (req, res, next) {
    let name
    let balance
    if (req.session.uid) {
        let user = nosql.getUserByUid(req.session.uid)
        name = user.name
        balance = await token.tokenBalance(user.Ethereum)
    }
    res.render('buy_token', { name, balance })
})

//投票頁面
router.get('/vote', async function (req, res, next) {
    let name
    let balance
    if (req.session.uid) {
        let user = nosql.getUserByUid(req.session.uid)
        name = user.name
        balance = await token.tokenBalance(user.Ethereum)
    }
    res.render('vote', { name, balance })
})

//計畫頁面
router.get('/project_information', async function (req, res, next) {
    let name
    let balance
        let user = nosql.getUserByUid(req.session.uid)
        name = user.name
        balance = await token.tokenBalance(user.Ethereum)
    let project = nosql.getProject("0000" + req.query.id)//商品暫時不超過10個，string 00001被轉成1
    let information = await token.getInformation("0000" + req.query.id)
    let donation = await token.projectTokenBalance(project.token, user.Ethereum);

    res.render('project_information', { name, balance, project, information, donation})
})

router.get('/project_information2', async function (req, res, next) {
    let name
    let balance
    if (req.session.uid) {
        let user = nosql.getUserByUid(req.session.uid)
        name = user.name
        balance = await token.tokenBalance(user.Ethereum)
    }
    res.render('project_information2', { name, balance })
})

router.get('/project_information3', async function (req, res, next) {
    let name
    let balance
    if (req.session.uid) {
        let user = nosql.getUserByUid(req.session.uid)
        name = user.name
        balance = await token.tokenBalance(user.Ethereum)
    }
    res.render('project_information3', { name, balance })
})

//我的專案
router.get('/my_project/:num', async function (req, res, next) {
    if (!req.session.uid) {
        res.redirect('/login')
        return
    }
    try {
        let user = nosql.getUserByUid(req.session.uid)
        let balance = await token.tokenBalance(user.Ethereum)
        let projects = await nosql.getProjectsByOwner(req.session.uid)
        let information = await token.getInformation(projects[req.params.num - 1].id)

        res.render('my_project', { name: user.name, balance, project: projects[req.params.num - 1], length: projects.length, information })
    }
    catch (e) {
        console.error(e)
        res.redirect('/')
    }
})

//追蹤頁面
router.get('/tracking', async function (req, res, next) {
    let name
    let balance
    if (req.session.uid) {
        let user = nosql.getUserByUid(req.session.uid)
        name = user.name
        balance = await token.tokenBalance(user.Ethereum)
    }
    var projects = require('../nosql/table/finishProject.json')
    var keyList = Object.keys(projects)
    var keyLength = keyList.length
    res.render('tracking', {
        pass: true,
        name: name,
        balance: balance,
        projects: projects,
        keyList: keyList,
        keyLength: keyLength
    })
})

//追蹤頁面
router.get('/watch', async function (req, res, next) {
    let name
    let balance
    if (req.session.uid) {
        let user = nosql.getUserByUid(req.session.uid)
        name = user.name
        balance = await token.tokenBalance(user.Ethereum)
    }
    res.render('watch', { name, balance })
})

//追蹤頁面
router.get('/tracking_project', async function (req, res, next) {
    let name
    let balance
    if (req.session.uid) {
        let user = nosql.getUserByUid(req.session.uid)
        name = user.name
        balance = await token.tokenBalance(user.Ethereum)
    }
    var projects = require('../nosql/table/finishProject.json')
    //console.log(projects["00006"].name);
    res.render('tracking_project', {
        name, balance,
        project: projects["0000" + req.query.id]//商品暫時不超過10個，string 00001被轉成1
    })
})

//追蹤頁面
router.get('/tracking_project02', async function (req, res, next) {
    let name
    let balance
    if (req.session.uid) {
        let user = nosql.getUserByUid(req.session.uid)
        name = user.name
        balance = await token.tokenBalance(user.Ethereum)
    }
    res.render('tracking_project02', { name, balance })
})

//追蹤頁面
router.get('/tracking_project03', async function (req, res, next) {
    let name
    let balance
    if (req.session.uid) {
        let user = nosql.getUserByUid(req.session.uid)
        name = user.name
        balance = await token.tokenBalance(user.Ethereum)
    }
    res.render('tracking_project03', { name, balance })
})

//階段性放款
router.get('/step', function (req, res, next) {
    res.render('step', { name, balance })
})

//登入
router.post('/login', function (req, res, next) {
    console.log(req.body)
    let result = nosql.login(req.body.uid)
    if (!result) {
        res.render('login', { data: "查無此帳號" })
    }
    else {
        if (result.psw == req.body.psw) {
            req.session.uid = req.body.uid
            req.session.name = result.name
            console.log(req.session.name)
            res.redirect('/')
        }
        else {
            res.render('login', { data: "密碼錯誤" })
        }
    }
})

//註冊
router.post('/signup', function (req, res, next) {
    console.log(req.body)
    if (!nosql.signup(req.body.uid, req.body.psw, req.body.name, req.body.Ethereum)) {
        res.redirect('signup')
    }
    else {
        req.session.name = req.body.name
        res.redirect('/')
    }
})

router.post('/create', function (req, res, next) {
    console.log(req.body)
    let {
        projectName
    } = req.body
    try {
        token.createProject(req.seesion.uid, projectName).then(function (receipt) {
            res.json(receipt)
        })
    }
    catch (e) {
        console.log(e)
        res.send(e)
    }
})

router.post('/donate', async function (req, res, next) {
    console.log(req.body)
    let {
        projectID,
        amount
    } = req.body
    try {
        token.contribute(req.session.uid, projectID, amount).then(function (receipt) {
            res.json(receipt)
        })
    }
    catch (e) {
        console.log(e)
        res.send(e)
    }
})
router.post('/withdraw', async function (req, res, next) {
    let {
        projectID
    } = req.body
    try {
        res.json(await token.withdraw(req.session.uid, projectID))
    }
    catch (e) {
        console.log(e)
        res.send(e)
    }
})

//查看交易receipt
router.get('/watch/:hash', async function (req, res, next) {
    web3.eth.getTransactionReceipt(req.params.hash).then(function (receipt) {
        res.json(receipt)
        //res.render('detail',{detail : receipt})
    })
})

router.post('/buyToken', async function (req, res, next) {
    console.log(req.body)
    let {
        amount
    } = req.body
    token.buyToken(req.session.uid, parseInt(amount, 10)).then(function (receipt) {
        res.json(receipt)
    })
})

router.post('/unlock', function (req, res, next) {
    console.log(req.body)
    let {
        password
    } = req.body
    let account = nosql.getUserByName(req.session.name).Ethereum
    console.log('account: ' + account)
    web3.eth.personal.unlockAccount(account, password, 6000).then(function () {
        res.send('Account unlocked!')
    })
})

router.get('/tokenBalance', async function (req, res, next) {
    console.log(req.query)
    let {
        name
    } = req.query
    let account = nosql.getUserByName(name).Ethereum
    console.log('account: ' + account)
    token.tokenBalance(account).then(function (receipt) {
        res.json(receipt)
    })
})

router.post('/voting', async function (req, res, next) {
    console.log(req.body)
    let {
        projectID,
        isAccept
    } = req.body
    try {
        token.vote(req.session.uid, projectID, isAccept=='true').then(function (receipt) {
            res.json(receipt)
        })
    }
    catch (e) {
        console.log(e)
        res.send(e)
    }
})

router.get('/getInformation', async function (req, res, next) {
    res.json(await token.getInformation(req.query.pid))
})

module.exports = router
