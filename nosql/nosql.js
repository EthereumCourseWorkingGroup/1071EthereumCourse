const fs = require('fs')

const user = require('./table/user.json')
const project = require('./table/project.json')

module.exports = {
    login(uid) {
        return user[uid]
    },
    signup(uid, psw, name, Ethereum) {
        if (uid in user) {
            return false
        }
        else {
            user[uid] = {
                psw,
                name,
                Ethereum,
            }
            fs.writeFileSync('./nosql/table/user.json', JSON.stringify(user, null, '\t'))
            return true
        }
    },
    getUserByUid(uid) {
        return user[uid]
    },
    getUserByName(name) {
        for (let uid in user) {
            if (user[uid].name == name) {
                return user[uid]
            }
        }
        return null
    },
    getProjects() {
        return project
    },
    getProjectsByOwner(uid) {
        let projects = []
        for (let pid in project) {
            if (project[pid].owner == uid) {
                projects.push(project[pid])
            }
        }
        return projects
    },
    getProject(pid) {
        return project[pid]
    },
    getFinishProjectsByOwner(address) {
        for (let pid in project) {
            if (project[pid].Ethereum == address) {
                return project[pid]
            }
        }
        return null
    },
    addProject(pid, name, content, imgURL, sponsor, date, amountPerStage, currentAmount, Ethereum) {
        project[pid] = {
            name,
            content,
            imgURL,
            sponsor,
            date,
            amountPerStage,
            currentAmount,
            Ethereum,
        }
        fs.writeFileSync('./nosql/table/project.json', JSON.stringify(project, null, '\t'))
    },
}