const web3 = require("./web3.js");
const contract = require("./contract.js");
const accounts = require("./accounts.json");

const nosql = require("../nosql/nosql.js");

let owner = accounts.paltform;
let t_address = accounts.platformToken;

//發行平台代幣 (發行者ethereum帳號)
function issueToken() {
  let token = new web3.eth.Contract(contract.token.abi);
  return token
    .deploy({
      data: contract.token.bytecode
    })
    .send({
      from: owner, // platform owner
      gas: "0x47e7c4",
      gasPrice: "0"
    })
    .then(function (newContractInstance) {
      return newContractInstance.options.address;
    });
}

//購買平台token (購買人id), 購買金額)
function buyToken(uid, amount) {
  let u_address = nosql.getUserByUid(uid).Ethereum;
  let token = new web3.eth.Contract(contract.token.abi, t_address);

  return token.methods.transfer(u_address, amount).send({
    from: owner,
    gas: "0x47e7c4",
    gasPrice: "0"
  });
}

//提出計畫 (募資者id, 計畫名稱)
function createProject(uid, name) {
  let u_address = nosql.getUserByUid(uid).Ethereum;
  let project = new web3.eth.Contract(contract.project.abi);

  return project
    .deploy({
      data: contract.project.bytecode,
      arguments: [0, 0, name, u_address, t_address]
    })
    .send({
      from: u_address, // project owner
      gas: "0x47e7c4",
      gasPrice: "0"
    })
    .then(function (newContractInstance) {
      return newContractInstance.options.address;
    });
}

//增加目標 (募資者id, 項目id, 目標金額)
function addGoal(uid, pid, amount) {
  let u_address = nosql.getUserByUid(uid).Ethereum;
  let p_address = nosql.getProject(pid).Ethereum;
  let project = new web3.eth.Contract(contract.project.abi, p_address);

  project.methods.addGoal(amount).send({
    from: u_address,
    gas: "0x47e7c4",
    gasPrice: "0"
  });
}

//捐款 (捐款者id, 項目id, 捐款數量)
function contribute(uid, pid, amount) {
  let u_address = nosql.getUserByUid(uid).Ethereum;
  let p_address = nosql.getProject(pid).Ethereum;
  let token = new web3.eth.Contract(contract.token.abi, t_address);
  let project = new web3.eth.Contract(contract.project.abi, p_address);

  token.methods.approve(p_address, amount).send({
    from: u_address,
    gas: "0x47e7c4",
    gasPrice: "0"
  });

  return project.methods.buyToken(amount).send({
    from: u_address,
    gas: "0x47e7c4",
    gasPrice: "0"
  });
}

//計畫取款 (募資者id, 項目id)
function withdraw(uid, pid) {
  let u_address = nosql.getUserByUid(uid).Ethereum;
  let p_address = nosql.getProject(pid).Ethereum;
  let project = new web3.eth.Contract(contract.project.abi, p_address);

  return  project.methods.withdraw().send({
    from: u_address,
    gas: "0x47e7c4",
    gasPrice: "0"
  });
}

//投票 (捐款者id, 項目id)
function vote(uid, pid, isAccept) {
  let u_address = nosql.getUserByUid(uid).Ethereum;
  let p_address = nosql.getProject(pid).Ethereum;
  let project = new web3.eth.Contract(contract.project.abi, p_address);

  return project.methods.vote(isAccept).send({
    from: u_address,
    gas: "0x47e7c4",
    gasPrice: "0"
  });
}

async function getInformation(pid) {
  let p_address = nosql.getProject(pid).Ethereum;
  let project = new web3.eth.Contract(contract.project.abi, p_address);

  let totalGoals = await project.methods.totalGoals().call();
  let goals = [];
  for (let i = 0; i < totalGoals; i++) {
    let goal = await project.methods.goals(i).call();
    await goals.push(goal);
  }
  let accGoals = [];
  for (let i = 0; i < totalGoals; i++) {
    let goal = await project.methods.accGoals(i).call();
    await accGoals.push(goal);
  }
  let nowGoal = await project.methods.nowGoal().call();
  let tokenRaised = await project.methods.tokenRaised().call();
  let totalContributer = await project.methods.totalContributer().call();
  let nowVote = await project.methods.nowVotes(nowGoal).call();
  let token = await project.methods.token().call();

  return (inf = {
    goals: goals,
    accGoals: accGoals,
    totalGoals: totalGoals,
    nowGoal: nowGoal,
    tokenRaised: tokenRaised,
    totalContributer: totalContributer,
    nowVote: nowVote,
    token: token
  });
}

function tokenBalance(account) {
  let token = new web3.eth.Contract(contract.token.abi, t_address);
  return token.methods.balanceOf(account).call();
}

function watchTransfer(uid, pid) {
  let u_address = nosql.getUserByUid(uid).Ethereum;
  let pt_address = nosql.getProject(pid).token;
  let ptoken = new web3.eth.Contract(contract.projectToken.abi, pt_address);
  
  return ptoken.getPastEvents('Transfer', {
    filter: {
      from: u_address
    },
    fromBlock: 0,
    toBlock: 'latest'
  }).then(function (events) {
    let results = []
    events.forEach(element => {
      let result = {}
      result.project = nosql.getProjectByEthereum(events[element].returnValues.to)
      result.amount = events[element].returnValues.value
      results.push(result)
    })
    return results
  })
}

function projectTokenBalance(pt_address, account) {
  let token = new web3.eth.Contract(contract.token.abi, pt_address);
  return token.methods.balanceOf(account).call();
}
/*
function watchEventsContribute(address){

    let project = new web3.eth.Contract(contract.project.abi, project_address);
    return project.getPastEvents('Contribute', {
        fromBlock: 0,
        toBlock: 'latest'
    })
}

function watchEventsExpense(address){

    let project = new web3.eth.Contract(contract.project.abi, project_address);
    return project.getPastEvents('Expense', {
        fromBlock: 0,
        toBlock: 'latest'
    })
}
*/

module.exports = {
  issueToken: issueToken,
  buyToken: buyToken,
  createProject: createProject,
  addGoal: addGoal,
  contribute: contribute,
  withdraw: withdraw,
  vote: vote,
  getInformation: getInformation,
  tokenBalance: tokenBalance,
  watchTransfer: watchTransfer,
  projectTokenBalance: projectTokenBalance
  //watchEventsContribute : watchEventsContribute,
  //watchEventsExpense, watchEventsExpense
};
