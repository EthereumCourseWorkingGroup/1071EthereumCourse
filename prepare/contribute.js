const token = require('../lib/token')

token.buyToken("test", 10000)
token.buyToken("nccu", 20000)
token.buyToken("hi", 10000)
token.buyToken("test", 500000)
token.buyToken("nccu", 500000)
token.buyToken("hi", 1000000)
token.buyToken("test", 400000)
token.buyToken("nccu", 200000)
token.buyToken("hi", 200000)


token.contribute("test", "00004", 10000)
token.contribute("nccu", "00004", 20000)
token.contribute("hi", "00004", 10000)

token.contribute("test", "00005", 500000)
token.contribute("nccu", "00005", 500000)
token.contribute("hi", "00005", 1000000)

token.contribute("test", "00006", 400000)
token.contribute("nccu", "00006", 200000)
token.contribute("hi", "00006", 200000)
