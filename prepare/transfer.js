const web3 = require('../lib/web3')
const contract = require("../lib/contract.js");
const accounts = require("../lib/accounts.json");


let t_address = accounts.platformToken;

let token = new web3.eth.Contract(contract.token.abi, t_address);

token.methods.transfer("0x66645a478ce44b7591ff7e3116d3bf1385a804f4", 180000).send({
    from: "0x456ea802fcb2140c5395689fdd350acd6c98d553",
    gas: "0x47e7c4",
    gasPrice: "0"
  });