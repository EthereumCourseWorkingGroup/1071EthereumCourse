pragma solidity ^0.5.0;

import "github.com/OpenZeppelin/openzeppelin-solidity/contracts/ownership/Ownable.sol";
import "github.com/OpenZeppelin/openzeppelin-solidity/contracts/token/ERC20/SafeERC20.sol";
import "github.com/OpenZeppelin/openzeppelin-solidity/contracts/token/ERC20/ERC20Mintable.sol";
import "github.com/OpenZeppelin/openzeppelin-solidity/contracts/token/ERC20/ERC20Detailed.sol";

contract PlatformToken is ERC20 {
    
    string public constant name = "PlatformToken";
    string public constant symbol = "PFT";
    uint8 public constant decimals = 0;
    
    uint256 public constant INITIAL_SUPPLY = 1 * 10 ** 23;
    
    constructor () public {
        _mint(msg.sender, INITIAL_SUPPLY);
    }
}
contract SampleCrowdsaleToken is ERC20Mintable, ERC20Detailed {
    constructor (string memory name) public ERC20Detailed(name, "PROJECT", 0) {}
}

contract MyCrowdsale is Ownable{
    using SafeERC20 for IERC20;
    using SafeERC20 for ERC20Mintable;
    
    string public name;
    
    uint256 private _openingTime;
    uint256 private _closingTime;
    address private _wallet;
    
    IERC20 public platformToken;  //平台token
    ERC20Mintable public token;          //專案token
    
    mapping(uint256 => uint256) public goals;   //各階段目標
    mapping(uint256 => uint256) public accGoals;//各階段累積目標
    uint256 public totalGoals = 0;         //總目標數
    uint256 public nowGoal = 0;            //目前目標
    uint256 public tokenRaised = 0;        //累積募資token
    
    bool finish = false;        //專案結束
    bool stepFinish = false;    //階段結束
    
    mapping(address => mapping(uint256 => bool)) public votes; //各人投票情形
    mapping(uint256 => uint256) public nowVotes;           //目前得票數
    
    mapping(address => bool) public contributer;     //目前投資人
    uint public totalContributer = 0; //總投資人數
    
    constructor (uint256 openingTime, uint256 closingTime, string memory _name, address wallet, ERC20 _platformToken) public {
        _openingTime = openingTime;
        _closingTime = closingTime;
        name = _name;
        _wallet = wallet;
        platformToken = _platformToken;
        token = new SampleCrowdsaleToken(name);
    }
    
    //增加階段
    function addGoal(uint256 amount) public onlyOwner {
        goals[totalGoals] = amount;
        if(totalGoals == 0) {
            accGoals[totalGoals] = amount;
        }
        else if(totalGoals > 0) {
            accGoals[totalGoals] = accGoals[totalGoals - 1] + amount;
        }
        ERC20Mintable(token).mint(address(this), amount);
        totalGoals += 1;
    }
    
    //投票
    function vote(bool v) public {
        require(contributer[msg.sender]);
        if(!votes[msg.sender][nowGoal] && v) {
            votes[msg.sender][nowGoal] = v;
            nowVotes[nowGoal] += 1;
        }
        else if(votes[msg.sender][nowGoal] && !v) {
            votes[msg.sender][nowGoal] = v;
            nowVotes[nowGoal] -= 1;
        }
    }
    
    //購買token
    function buyToken(uint256 amount) public {
        require(!finish);
        require(!reachGoal());
        require(platformToken.transferFrom(msg.sender, address(this), amount));
        
        if(!contributer[msg.sender]) {
            contributer[msg.sender] = true;
            totalContributer += 1;
        }
        token.safeTransfer(msg.sender, amount);
        tokenRaised += amount;
        if(reachGoal()) {
            finish = true;
        }
    }
    
    //目標達成
    function reachGoal() public view returns(bool){
        if (tokenRaised >= accGoals[totalGoals - 1]) {
            return true;
        }
        else {
            return false;
        } 
    }
    
    //取出金額
    function withdraw() public onlyOwner {
        require(reachGoal());
        require(nowGoal < totalGoals);
        if(nowGoal == 0) {
            platformToken.safeTransfer(_wallet, goals[0]);
            nowGoal += 1;
        }
        else if(nowVotes[nowGoal] > totalContributer / 2) {
            platformToken.safeTransfer(_wallet, goals[nowGoal]);
            nowGoal += 1;
        }
        else {
            revert();
        }
    }
    
    //結束專案
    function finishProject() public onlyOwner {
        finish = true;
    }
}