let donateButton = $('#donateButton');

donateButton.on('click', async function () {
    let projectID = $('#projectID')[0].value;
    let amount = $('#amount')[0].value;
    // 解鎖
    unlockAccount().then(result => {
        console.log(result)
        if (result) {
            $.post('/donate', {
                projectID: projectID,
                amount: amount
            }, function (res) {
                if (res.status) {
                    swal("捐款成功", {
                        icon: "success",
                    });
                } else {
                    swal("捐款失敗", {
                        icon: "error",
                    });
                }
            })
        }
    })
})

async function unlockAccount() {
    return swal("請輸入你的密碼:", {
        content: {
            element: "input",
            attributes: {
                placeholder: "Type your password",
                type: "password",
            },
        },
    })
        .then((value) => {
            if (value == null) {
                return false;
            }
            else {
                return $.post('/unlock', {
                    password: value
                })
                    .then(function (result) {
                        if (result == 'Account unlocked!') {
                            return swal("密碼正確", {
                                icon: "success",
                            }).then(function () {
                                return true;
                            })
                        }
                        else {
                            return swal("密碼錯誤", {
                                icon: "error",
                            }).then(function () {
                                return false;
                            })
                        }
                    })
            }
        })
}

