let buyAmount = $('#buyAmount')[0].value;
let buyButton = $('#buyButton');
let name = $('#name')[0].value;


//buying token
buyButton.on('click', async function () {
    buyAmount = $('#buyAmount')[0].value;
    // 解鎖
    unlockAccount().then(result => {
        console.log(result)
        if (result) {
            console.log('buying...')
            $.post('/buyToken', {
                amount: buyAmount
            }, function (res) {
                if (res.status) {
                    $.get('/tokenBalance', {
                        name: name
                    }, function (res) {
                        if (res) {
                            console.log(name + '已持有代幣:' + res)
                            swal("Success! Now you have " + res, {
                                icon: "success",
                            }).then(function(){
                                location.reload()
                            })
                        } else {
                            swal("Fail! " + res, {
                                icon: "error",
                            });
                        }
                    })
                } else {
                    swal("Fail! " + res, {
                        icon: "error",
                    });
                }
            })
        }
    })
})

async function unlockAccount() {
    return swal("請輸入你的密碼:", {
        content: {
            element: "input",
            attributes: {
                placeholder: "Type your password",
                type: "password",
            },
        },
    })
        .then((value) => {
            if (value == null) {
                return false;
            }
            else {
                return $.post('/unlock', {
                    password: value
                })
                    .then(function (result) {
                        if (result == 'Account unlocked!') {
                            return swal("密碼正確", {
                                icon: "success",
                            }).then(function () {
                                return true;
                            })
                        }
                        else {
                            return swal("密碼錯誤", {
                                icon: "error",
                            }).then(function () {
                                return false;
                            })
                        }
                    })
            }
        })
}

