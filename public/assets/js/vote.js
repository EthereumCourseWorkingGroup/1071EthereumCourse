let projectID = $('#projectID')[0].value;
let acceptButton = $('#acceptButton');
let denyButton = $('#denyButton');
let name = $('#name')[0].value;

acceptButton.on('click', async function () {
    projectID = $('#projectID')[0].value;
    console.log(projectID + "    " + true)
    // 解鎖
    unlockAccount().then(result => {
        console.log(result)
        if (result) {
            $.post('/voting', {
                projectID: projectID,
                isAccept: true
            }, function (res) {
                if (res.status) {
                    swal("投票成功", "您投了贊成", {
                        icon: "success",
                    });
                } else {
                    swal("投票失敗", {
                        icon: "error",
                    });
                }
            })
        }
    })
})
denyButton.on('click', async function () {
    projectID = $('#projectID')[0].value;
    console.log(projectID + "    " + false)
    // 解鎖
    unlockAccount().then(result => {
        console.log(result)
        if (result) {
            $.post('/voting', {
                projectID: projectID,
                isAccept: false
            }, function (res) {
                if (res.status) {
                    swal("投票成功", "您投了反對", {
                        icon: "success",
                    });
                } else {
                    swal("投票失敗", {
                        icon: "error",
                    });
                }
            })
        }
    })
})



async function unlockAccount() {
    return swal("請輸入你的密碼:", {
        content: {
            element: "input",
            attributes: {
                placeholder: "Type your password",
                type: "password",
            },
        },
    })
        .then((value) => {
            if (value == null) {
                return false;
            }
            else {
                return $.post('/unlock', {
                    name: name,
                    password: value
                })
                    .then(function (result) {
                        if (result == 'Account unlocked!') {
                            return swal("密碼正確", {
                                icon: "success",
                            }).then(function () {
                                return true;
                            })
                        }
                        else {
                            return swal("密碼錯誤", {
                                icon: "error",
                            }).then(function () {
                                return false;
                            })
                        }
                    })
            }
        })
}

